function PlaceHold(h=250,w=300,parseSelector=[]){
  this.copyright="張睿玹 DEV Team(Chang JuiHsuan DEV Team)";
  this.version="0.0.1 B";
  this.parseList=[
    ...parseSelector,
    "#parsePL",
    "placehold-img"
  ]
  this._h=h;
  this._w=w;
  this.libName="PlaceHoldPL";
  this.HTTP_OR_HTTPS=location.protocol
  this.init()
  
}
PlaceHold.prototype.init=function(){
  let msg=this.libName+" JS Lib loaded"
  msg+="\n"
  msg+="version: "+this.version
  console.log(msg)
}
PlaceHold.prototype.error=function(code,msg=""){
  /*
  List Of error code
  
  */
  if(code.startsWith("E")){
    //Like E2213 : E=Exit
    throw new Error(this.libName+" Error"+code+": "+this.errorList[code]+msg)
  }else{
    //Like K1029 : K=Keep
    console.log(this.libName+" Error"+code+": "+this.errorList[code]+msg)
  }
}
PlaceHold.prototype.errorList={
  "K0089":"Tag didn't be placeholded , tagName:"
}
PlaceHold.prototype.placeIMG = function(weight,height,type="HTML"){
  var rtn;
  switch(type){
    case "HTML":
      var s=document.createElement("img")
      s.src=this.placeIMG(weight,height,"src")
      rtn=s;
      break;
    case "src":
      var s=this.HTTP_OR_HTTPS+"//placehold.it/"+weight+"x"+height+".png";
      rtn=s
      break;
  }
  return rtn;
}
PlaceHold.prototype.place = function(parseSelector=[],w,h){
  if(typeof parseSelector=="string"){
    let _ps=[]
    _ps.push(parseSelector)
    parseSelector=_ps
  }
  for(var i=0;i<parseSelector.length;i++){
    var elem=document.querySelectorAll(parseSelector[i])
    if(elem){
    
    for(var k=0;k<elem.length;k++){
      switch(elem[k].tagName){
        case "DIV":
          elem[k].appendChild(this.placeIMG(w,h,"HTML"))
          break;
        case "IMG":
          elem[k].setAttribute("src",this.placeIMG(w,h,"src"))
          break;
        case "PLACEHOLD-IMG":
          elem[k].appendChild(this.placeIMG(w,h,"HTML"))
          break;
        default :
          elem[k].appendChild(this.placeIMG(w,h,"HTML"))
      }
    }
    
    }//end if elem
    
    
  }
}
PlaceHold.prototype.default = function(){
  for(var i=0;i<this.parseList.length;i++){
    var elem=document.querySelectorAll(this.parseList[i])
    
    if(elem){
    
    for(var k=0;k<elem.length;k++){
      //check if it set data attr
      let w,h;
      if(elem[k].dataset.width){
        w=elem[k].dataset.width
      }else{
        w=this._w
      };
      if(elem[k].dataset.height){
        h=elem[k].dataset.height
      }else{
        h=this._h
      }
      //end check
      switch(elem[k].tagName){
        case "DIV":
          elem[k].appendChild(this.placeIMG(w,h,"HTML"))
          break;
        case "IMG":
          elem[k].setAttribute("src",this.placeIMG(w,h,"src"))
          break;
        case "PLACEHOLD-IMG":
          elem[k].appendChild(this.placeIMG(w,h,"HTML"))
          break;
        default :
          elem[k].appendChild(this.placeIMG(w,h,"HTML"))
      }
    }
    
    }//end if elem
    
    
  }
}
var $PL=new PlaceHold(100,200)
window.addEventListener("load",function(){
  $PL.default()
})