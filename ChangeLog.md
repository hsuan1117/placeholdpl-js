# Change Log of PlaceHoldPL JS Library

**Version: 0.0.1 A**

+ Set default selector
+ First version
+ @2019.01.13

**Version: 0.0.1 B**

+ Add automatic check Http or Https
+ Add some `;` to prevent code error in some browsers
+ Deprecated Code (will be deleted in *0.0.1 C*) 
+ `var $PL=new PlaceHold(100,200)
    window.addEventListener("load",function(){
        $PL.default()
     })`
+ @2019.01.14

**Version: 0.0.1 B1**

+ Change Protocal Method (because it will get an error in some protocols)
+ such as "ftp://" , "file://" 